# Ghost Archiver

This tool archives published posts from a [Ghost blog](https://ghost.org/) in a variety of formats including .html bundled with assets. The intention is to generate a [human readable archive](#html-archives). You may also export posts for consumption by [Hugo](https://gohugo.io/), a static site generator. This last function is [described more below](#hugo).

## Installation
1. Clone this repository
    * `git clone git@gitlab.com:aaronparr/ghost-archiver.git`
2. Enter the respository
    * `cd ghost-archiver`
3. Install packages
    * `npm install`
4. Input your Ghost Blog's Content API credentials to the **.env** file. See [Credentials](#credentials).

## Credentials
You need to provide a URL and Key to Ghost's Content API. For more info on obtaining these see [Ghost's Documentation](https://ghost.org/docs/content-api/)

I recommend you store both of these in the **.env** file in this directory. If it doesn't exist `npm install` should create a blank one.

After you edit **.env**, the contents will look similar to:
```
GHOST_URL=http://localhost:2368
GHOST_KEY=0e08eb74bb4a38b846044094a1
```
BUT the values for GHOST_URL and GHOST_KEY will match your particular blog. The values in the **.env** serve as defaults when using the tool. But you may also specify the URL and Key through arguments on the command line, and this will take precedence over the defaults.

* `npm run archiver url http://localhost:2368 key 0e08eb74bb4a38b846044094a1`

## Usage

1. Enter the repository
2. `npm run archiver`

To see options available on the command line:
* `npm run archiver help`

To use unix like flags for your options, invoke the script directly:
* `./archiver.js --help`
* `./archiver.js -t md -c month`

Or add NPM's `--` at the start of the line:
* `npm run archiver -- -h`
* `npm run archiver -- --type md --catalog month`

## HTML Archives

The initial intent was to archive the posts of a Ghost Blog to HTML bundled with assets (images etc...) which would still be readable by a human after the original blog is gone. Similar to a web scraped archive, but cleaner. And that's what the tool does by default.

* `npm run archiver`

Export HTML, plaintext, and assets (like images) bundled by post in a directory tree based on the year / month / day the post was published:

* `npm run archiver type html txt catalog day`

Bundle HTML posts and their assets (like images) by year rather than by post:

* `npm run archiver catalog year flat`

### Note:
You also have the option to change the style sheet used by the HTML. There is a user configurable stylesheet named **user_config/style.css**. You can put whatever CSS you want in that file. The default I included is essentially minified Caspar. The tool copies this file into the root of the archive so if you move the HTML it will not be able to locate the stylesheet.

## HUGO
This tool also exports files for use by HUGO.

* `npm run archiver type hugo`

The path for these files in the archive is: **_hugo/posts/**

Edit **user_config/hugo.js** to configure the hugo files output by the archiver. Keep the example file **user_config/hugo_example.js** as a reference and a fall back should you get lost. However as long as you maintain the parameters of the configurable functions (arguments and return values) you should be fine.

### Note:
While customizing your frontmatter, you might want to reference the JSON from Ghost. You can export an archive of JSON files one for each post with the option `type json` as follows:

* `npm run archiver type json dir ~/Desktop/ghost_json_only`

 With this command we also named the archive **ghost_json_only** using the `dir` option and sent it to the **Desktop**.

## Deficiencies

This tool does not archive posts of the page type nor does it allow you to alter ghost's filters from the command line because I don't currently have a need for either of these features. The initial intention was only to archive all published posts in a human readable format before shutting down the server. I did a fair bit more than this.

### Deficiencies that May Result in Future Improvements
* Links within the body of a post to other posts are not updated to work within the archive. I have been thinking of generating a site map which would also solve this.

* The default style and HTML wrapper for the HTML archive does not allow the playing of audio via Ghost's audio widget. I need to bogart some of Ghost's js like I did the style.

* Assets like images and video that are stored outside the domain of your blog are not scraped for the HTML archive. I have been thinking of expanding this to other domains set by the user.

* The markdown conversion chokes on video and audio cards. I could preprocess those myself.