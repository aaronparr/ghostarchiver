#!/usr/bin/env node

const GhostContentAPI = require('@tryghost/content-api');
var TurndownService = require('turndown');
const fs = require('fs-extra');
const path = require('path');
const http = require('http');
const https = require('https');
const EL = require('os').EOL;

const hugo = require('./user_config/hugo.js');
const C = require('./util/cliColors.js').Colors;

// if I ever convert this to a module change this
// loads .env vars into our process.env
if(process.env.NODE_ENV!=='production')
{
  require('dotenv').config();
}


// if we update this for a newer API we should adjust this
const ghostVersion = 'v5.24';

// CLI arguments and their options
const COMMANDS =
[ "help","-h","--help",
  "url","-u","--url",
  "key","-k","--key",
  "dir","-d","--dir",
  "type","-t","--type",
  "catalog","-c","--catalog"
]
const OPTS =
{
  "type": ["html","md","txt","json","assets","hugo"],
  "catalog": ["flat","content","tag","year","month","day","mmdd"]
};
const ASSET_TYPES = ["images","files","media"]

// merely used in text
const getTimeStamp = () =>
{
  const date = new Date();
  const timestamp = 
      date.getFullYear()+"_"
    +(date.getMonth()+1).toString().padStart(2, '0')+"_"
    + date.getDate().toString().padStart(2, '0')+"-"
    + date.getHours().toString().padStart(2, '0')+"_"
    + date.getMinutes().toString().padStart(2, '0')+"_"
    + date.getSeconds().toString().padStart(2, '0')
  ;

  return timestamp;
}


// this function handles limitations with the turndown package
// solution is to preprocess the code
const processHtmlForMarkdown = (htmlSource) =>
{
  let htmlReturned = htmlSource;

  // FILE DOWNLOADS
  // links containing block elements get stretched over multiple lines
  // pull request solution awaiting merge: https://github.com/mixmark-io/turndown/pull/419
  // first match the whole block
  let regex =  RegExp(/(<a.*?"kg-file-card-container".*?download>[\S\s]*?"kg-file-card-title">.*?<\/div>[\s\S]*?<\/a>)/, "g");
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    // save the html before and after the block
    let splitHtml = htmlReturned.split(match[1]);
    // pull what we want from it
    let innerRegex =  RegExp(/(<a.*?"kg-file-card-container".*?download>)[\S\s]*?"kg-file-card-title">(.*?)<\/div>[\s\S]*?(<\/a>)/, "g");
    while ( (innerMatch = innerRegex.exec( match[1] )) !== null)
    {
      // add it back together
      htmlReturned = splitHtml[0]+innerMatch[1]+innerMatch[2]+innerMatch[3]+splitHtml[1];
    }
  }

  // update source here
  htmlSource = htmlReturned;

  // LINKS
  // similar problem to what was identified above
  // first match the whole block
  regex =  RegExp(/(<a .*?"kg-bookmark-container".*?>[\S\s]*?<\/a>)/, "g");
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    // save the html before and after the block
    let splitHtml = htmlReturned.split(match[1]);
    // pull what we want from it
    let innerRegex =  RegExp(/(<a .*?"kg-bookmark-container".*?>)[\S\s]*?"kg-bookmark-title">(.*?)<\/div><div class="kg-bookmark[\s\S]*?(<\/a>)/, "g");
    while ( (innerMatch = innerRegex.exec( match[1] )) !== null)
    {
      // add it back together
      htmlReturned = splitHtml[0]+innerMatch[1]+innerMatch[2]+innerMatch[3]+splitHtml[1];
    }
  }

  // update source here if we add another regex
  // htmlSource = htmlReturned;

  return htmlReturned;
}

// crawl over the html and extract the URLs from every img tag's src property
// and do the same for files uploaded to ghost
// puts the resource URLs into an array of objects
// { type: 'images', url: 'https://...'  }
const getAllFileRefsFromHtml = (htmlSource) =>
{
  let fileRefs = [];

  // images in img src=
  let regex =  RegExp('<img.*?src=[\'"]([^ \'\"\?]+).*?[\'"]', 'g');
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    if(!fileRefs?.url?.includes(match[1]))
    {
      fileRefs.push( {type: 'images', url: match[1]} );
    }
  }

  // imgs in srcset
  // srcset="http://localhost:2368/content/images/size/w600/2022/12/20080913.JPG 600w, http://localhost:2368/content/images/size/w1000/2022/12/20080913.JPG 1000w, http://localhost:2368/content/images/size/w1600/2022/12/20080913.JPG 1600w, http://localhost:2368/content/images/size/w2400/2022/12/20080913.JPG 2400w"
  // first grab everything in the quotes then iterate over each image url between them on a second pass
  regex =  RegExp('<img.*?srcset=[\'"]([^\'\"]+)*?[\'"]', 'g');
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    let innerRegex =  RegExp('([^ \'\"\?]+) [0-9]*w', 'g');
    while ( (innerMatch = innerRegex.exec( match[1] )) !== null)
    {
      if(!fileRefs?.url?.includes(innerMatch[1]))
      {
        fileRefs.push( {type: 'images', url: innerMatch[1]} );
      }
    }
  }

  // background images
  regex =  RegExp('style=[\'\"]background.*?url.[\'"]([^ ^\'^\"]+)[\'\"]', 'g');
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    if(!fileRefs?.url?.includes(match[1]))
    {
      fileRefs.push( {type: 'images', url: match[1]} );
    }
  }

  // documents uploaded to ghost
  // <a class="kg-file-card-container" href="http://localhost:2368/content/files/2022/12/vsgp_primer-1.pdf" title="Download" download></a>
  regex =  RegExp('<a.*?class="kg-file-card-container".*?href=[\'"](.*?)[\'"]', 'g');
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    if(!fileRefs?.url?.includes(match[1]))
    {
      fileRefs.push( {type: 'files', url: match[1]} );
    }
  }

  // audio uploaded to ghost
  // <audio src="http://localhost:2368/content/media/2022/12/Sorry---7-28-22--10.11-PM.mp3" preload="metadata"></audio>
  regex =  RegExp('<audio.*?src=[\'"](.*?)[\'"]', 'g');
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    if(!fileRefs?.url?.includes(match[1]))
    {
      fileRefs.push( {type: 'media', url: match[1]} );
    }
  }

  // video uploaded (also includes an image)
  // <video src="http://localhost:2368/content/media/2022/12/My-Movie.mp4" poster="https://img.spacergif.org/v1/1920x1080/0a/spacer.png" width="1920" height="1080" loop autoplay muted playsinline preload="metadata" style="background: transparent url('http://localhost:2368/content/images/2022/12/media-thumbnail-ember1859.jpg') 50% 50% / cover no-repeat;" /></video>
  regex =  RegExp('<video.*?src=[\'"](.*?)[\'"]', 'g');
  while ( (match = regex.exec( htmlSource )) !== null)
  {
    if(!fileRefs?.url?.includes(match[1]))
    {
      fileRefs.push( {type: 'media', url: match[1]} );
    }
  }

  // // all links
  // regex =  RegExp('(http[^ ^\'^\"]+)', 'g');
  // while ( (match = regex.exec( htmlSource )) !== null)
  // {
  //   if(!fileRefs?.url?.includes(match[1]))
  //   {
  //     console.log(match[1]);
  //     fileRefs.push( {type: 'misc', url: match[1]} );
  //   }
  // }

  // // brute force
  // // all images stored in ghost
  // regex =  RegExp('([^ ^\'^\"]+/content/images/[^ ^\'^\"]+)', 'g');
  // while ( (match = regex.exec( htmlSource )) !== null)
  // {
  //   if(!fileRefs?.url?.includes(match[1]))
  //   {
  //     fileRefs.push( {type: 'images', url: match[1]} );
  //   }
  // }

  // // brute force
  // // all files in ghost
  // regex =  RegExp('([^ ^\'^\"]+/content/files/[^ ^\'^\"]+)', 'g');
  // while ( (match = regex.exec( htmlSource )) !== null)
  // {
  //   if(!fileRefs?.url?.includes(match[1]))
  //   {
  //     fileRefs.push( {type: 'files', url: match[1]} );
  //   }
  // }

  // // brute force
  // // all media in ghost
  // regex =  RegExp('([^ ^\'^\"]+/content/media/[^ ^\'^\"]+)', 'g');
  // while ( (match = regex.exec( htmlSource )) !== null)
  // {
  //   if(!fileRefs?.url?.includes(match[1]))
  //   {
  //     fileRefs.push( {type: 'media', url: match[1]} );
  //   }
  // }

  return fileRefs;
}


// triggered by the arguments -h | help
const displayHelp = () =>
{
  console.log(
    EL+C.Bright+"GHOST ARCHIVER: (Compatible with API "+ghostVersion+" )"+C.Reset+EL
   +EL
   +"This tool archives posts from a Ghost blog."+EL
   +"Default usage produces HTML bundled wtih assets."+EL
   +"For additional options see below."+EL
   +""+EL
   +C.Bright+"USAGE:"+C.Reset+EL
   +C.Underscore+"ARG"+C.Reset+"                  "+C.Underscore+"MEANING"+C.Reset+EL
   +"[-u|"+C.Bright+"url"+C.Reset+" <URL>]       Specify the url to the blog."+EL
   +"                       "+C.Dim+"-u http://localhost:2368"+C.Reset+EL
   +"[-k|"+C.Bright+"key"+C.Reset+" <KEY>]       Specify the key for the Content API."+EL
   +"                       "+C.Dim+"-k 0e08ec34bb4a38b046000094a1"+C.Reset+EL
   +"[-d|"+C.Bright+"dir"+C.Reset+" <PATH>]      <PATH> for the archive, ending in its name."+EL
   +"                       "+C.Dim+"-d ~/Desktop/my_archive_name"+C.Reset+EL
   +"[-t|"+C.Bright+"type"+C.Reset+" <OPTS>]     <OPTS> one or more, each separated by a space:"+EL
   +"                       "+C.Bright+"html"+C.Reset+"    : .html type. With post assets. (Default)"+EL
   +"                       "+C.Bright+"md"+C.Reset+"      : .md type. Minimal frontmatter and markdown."+EL
   +"                       "+C.Bright+"txt"+C.Reset+"     : .txt type. Only plaintext content."+EL
   +"                       "+C.Bright+"json"+C.Reset+"    : .json type. Each post as json."+EL
   +"                       "+C.Bright+"assets"+C.Reset+"  : Post assets."+EL
   +"                       "+C.Bright+"hugo"+C.Reset+"    : Posts and assets for consumption by Hugo."+EL
   +"[-c|"+C.Bright+"catalog"+C.Reset+" <OPTS>]  <OPTS> one or more, each separated by a space:"+EL
   +"                       "+C.Bright+"flat"+C.Reset+"    : terminal directory named after post is eliminated."+EL
   +"                       "+C.Bright+"content"+C.Reset+" : a branch for each content type (as specified above)."+EL
   +"                       "+C.Bright+"tag"+C.Reset+"     : a branch for each primary tag."+EL
   +"                       "+C.Bright+"year"+C.Reset+"    : post paths include YYYY"+EL
   +"                       "+C.Bright+"month"+C.Reset+"   : post paths include YYYY/MM"+EL
   +"                       "+C.Bright+"mmdd"+C.Reset+"    : post paths include YYYY/MMDD"+EL
   +"                       "+C.Bright+"day"+C.Reset+"     : post paths include YYYY/MM/DD"+EL
   +"[-h|"+C.Bright+"help"+C.Reset+"]            Display this message."+EL
   +""+EL
 );
}

const deleteEmptyDirectoriesInList = (directoriesToClean) =>
{
  let emptyDirectoriesRemaining = [];

  directoriesToClean.forEach( directory =>
  {
    deleteDirectory(directory, true);
    // the assets tree gets really messy.
    // we clean it up here
    if(directory.includes("assets"))
    {
      let newDirectory = directory.substring(0, directory.lastIndexOf("/"));
      var files = fs.readdirSync(newDirectory);
      if( files.length==0 && !emptyDirectoriesRemaining.includes(newDirectory) )
      {
          emptyDirectoriesRemaining.push(newDirectory);
      }
    }
  });

  if(emptyDirectoriesRemaining.length)
  {
    deleteEmptyDirectoriesInList(emptyDirectoriesRemaining);
  }
}

const deleteDirectory = (dirPath, forceDeletion) =>
{
  fs.rmSync(dirPath, { recursive: true, force: forceDeletion })
}


/// ---------------------------------------------------------------------------
// this is the "main" - it executes when the scrip is called.

const archiveGhostBlog = async () =>
{
  // ------------------------
  // initialize
  // ghost api creds
  let ghostURL = process.env.GHOST_URL;
  let ghostKey = process.env.GHOST_KEY;
  // archive directory
  const timestamp = getTimeStamp();
  let archivePathBase = path.join(__dirname, 'archive_ghost');
  // type options
  let TYPE_HTML         = true;
  let TYPE_TXT          = false;
  let TYPE_JSON         = false;
  let TYPE_MD           = false;
  let TYPE_HUGO         = false;
  let TYPE_ASSETS       = false;
  // archive options
  let ARCHIVE_FLAT      = false;
  let ARCHIVE_SEPARATE  = false;
  let ARCHIVE_YEAR      = false;
  let ARCHIVE_MONTH     = false;
  let ARCHIVE_DAY       = false;
  let ARCHIVE_MMDD      = false;
  let ARCHIVE_TAG       = false;

  // pull in the user's style sheet
  const userStyleSheetPath = path.join(__dirname, 'user_config/style.css');

  // -------------------
  // handle options
  const args = process.argv.slice(2);
  if (args.length)
  {
    // console.log(args);
    if (args.includes("help")||args.includes("-h")||args.includes("--help"))
    {
      displayHelp();
      process.exit(0);
    }

    if (args.includes("url")||args.includes("-u")||args.includes("--url"))
    {
      let index = args.indexOf("url");
      if (index == -1)
      {
        index = args.indexOf("-u");
        if (index == -1)
          index = args.indexOf("--url");
      }

      let next = args[++index];
      if (next.indexOf('http')!=0 || COMMANDS.includes(next) )
      {
        console.error(EL+C.Red+"FAILED:"+C.Reset+EL+"Url "+next+" is invalid."+EL);
        process.exit(1);
      }
      else
      {
        args.splice(index, 1);
        ghostURL = next;
      }
    }

    if (args.includes("key")||args.includes("-k")||args.includes("--key"))
    {
      let index = args.indexOf("key");
      if (index == -1)
      {
        index = args.indexOf("-k");
        if (index == -1)
          index = args.indexOf("--key");
      }
 
      let next = args[++index];
      let regex =  RegExp('^[a-zA-Z0-9]', 'g');
      if(COMMANDS.includes(next) || next.length < 26 || regex.test(next))
      {
        console.error(EL+C.Red+"FAILED:"+C.Reset+EL+"Key "+next+" is invalid."+EL);
        process.exit(1);
      }
      else
      {
        args.splice(index, 1);
        ghostKey = next;
      }
    }

    if (args.includes("dir")||args.includes("-d")||args.includes("--dir"))
    {
      let index = args.indexOf("dir");
      if (index == -1)
      {
        index = args.indexOf("-d");
        if (index == -1)
          index = args.indexOf("--dir");
      }

      args.splice(index, 1);
      next = args[index];
      // illegal characters?
      let regex =  RegExp('[\?\%\*\:\|\"\'\<\>]', 'g');
      if (regex.test(next)) {
        // warn the user that their archive name was invalid and we are proceeding with default
        console.warn(EL+C.Red+"WARNING:"+C.Reset+EL+next+" includes illegal characters. Avoid ? % * : | \" \' < >"+EL+archivePathBase+" will be used instead."+EL);
      } else {
        // change to the new path
        archivePathBase = next;
        args.splice(index, 1);
      }
    }

    if (args.includes("type")||args.includes("-t")||args.includes("--type"))
    {
      let index = args.indexOf("type");
      if (index == -1)
      {
        index = args.indexOf("-t");
        if (index == -1)
          index = args.indexOf("--type");
      }
      // since we are being explicit, let's remove this
      TYPE_HTML = false;
      args.splice(index, 1);
      while (OPTS["type"].includes(args[index]))
      {
        if(args[index]=="html") {
          TYPE_HTML = true;
          args.splice(index, 1);
        } else if (args[index]=="txt")  {
          TYPE_TXT = true;
          args.splice(index, 1);
        } else if (args[index]=="md")  {
          TYPE_MD = true;
          args.splice(index, 1);
        } else if (args[index]=="json")  {
          TYPE_JSON = true;
          args.splice(index, 1);
        } else if (args[index]=="assets")  {
          TYPE_ASSETS = true;
          args.splice(index, 1);
        } else if (args[index]=="hugo")  {
          TYPE_HUGO = true;
          args.splice(index, 1);
        } else {
          break;
        }
      }

      if (!TYPE_HTML && !TYPE_MD && !TYPE_TXT && !TYPE_JSON && !TYPE_HUGO && !TYPE_ASSETS)
      {
        console.warn(EL+C.Red+"WARNING:"+C.Reset+EL+"No valid option for type. Proceeding with default behavior."+EL
        +"Allowed options ("+OPTS.type.join(' ')+")"+EL);
        TYPE_HTML = true;
      }
    }

    if (args.includes("catalog")||args.includes("-c")||args.includes("--catalog"))
    {
      let index = args.indexOf("catalog");
      if (index == -1)
      {
        index = args.indexOf("-c");
        if (index == -1)
          index = args.indexOf("--catalog");
      }

      args.splice(index, 1);
      while (OPTS["catalog"].includes(args[index]))
      {
        if(args[index]=="content") {
          ARCHIVE_SEPARATE = true;
          args.splice(index, 1);
        } else if (args[index]=="year")  {
          ARCHIVE_YEAR = true;
          args.splice(index, 1);
        } else if (args[index]=="month")  {
          ARCHIVE_MONTH = true;
          args.splice(index, 1);
        } else if (args[index]=="day")  {
          ARCHIVE_DAY = true;
          args.splice(index, 1);
        } else if (args[index]=="mmdd")  {
          ARCHIVE_MMDD = true;
          args.splice(index, 1);
        } else if (args[index]=="tag")  {
          ARCHIVE_TAG = true;
          args.splice(index, 1);
        } else if (args[index]=="flat")  {
          ARCHIVE_FLAT = true;
          args.splice(index, 1);
        } else {
          break;
        }
      }

      if (!ARCHIVE_SEPARATE && !ARCHIVE_YEAR && !ARCHIVE_MONTH && !ARCHIVE_DAY && !ARCHIVE_TAG && !ARCHIVE_FLAT && !ARCHIVE_MMDD)
      {
        console.warn(EL+C.Red+"WARNING:"+C.Reset+EL+"No valid option for catalog. Proceeding with default behavior."+EL
        +"Allowed options ("+OPTS.archive.join(' ')+")"+EL
        );
      }
    }
    if(args.length)
    {
      console.warn(EL+C.Red+"WARNING:"+C.Reset+EL+"Bad args ignored ("+args.join(' ')+"). Allowed args ("+COMMANDS.join(' ')+")"+EL+EL
      +C.Red+"Note:"+C.Reset+EL+"npm strips flagged args like -c or --help. To use them with npm run archiver, prepend the arguments with -- like this:"+EL
      +"   "+C.Yellow+"npm run archiver -- -t hugo json txt -c separate"+C.Reset+EL
      );
    }
  }

  // check for missing URL and Key
  if(ghostURL=="" || typeof ghostURL=="undefined" || ghostURL==null || ghostKey=="" || typeof ghostKey=="undefined" || ghostKey==null)
  {
    console.error(EL+C.Red+"FAILED:"+C.Reset+EL+"You need to set GHOST_URL and GHOST_KEY in your .env file."+EL+"For further information see https://ghost.org/docs/content-api/"+EL)
    process.exit(1);
  }

  // finish init and
  // get an instance of the api
  let api = new GhostContentAPI({
    url: ghostURL,
    key: ghostKey,
    version: ghostVersion
  });

  // we maintain a list of directories
  // any remaining in this list after all posts are processed should be deleted
  let directoriesToClean = [];

  // ------------------
  // extract some
  // posts
  console.time('All posts exported in');
  try
  {
    // Fetch the posts from the Ghost Content API
    const posts = await api.posts.browse(
    {
        limit: 'all',
        include: 'tags,authors',
        formats: ['html,plaintext'],
    });

    // this is the archive directory into which we are putting all the files
    archivePathBase = archivePathBase+'-'+timestamp
    fs.mkdirSync(archivePathBase, { recursive: true });

    // copy stylesheet into archivePathBase
    if(TYPE_HTML)
    {
      fs.copyFileSync(userStyleSheetPath, path.join(archivePathBase,'style.css'));
    }
      

    // to see the schema
    // console.log(posts[0]);
    // process each post one at a time
    await Promise.all(posts.map(async (post) =>
    {
      // hugo also uses this so we need to set it up out here
      // it is altered in the HTML section when we extract resources
      let htmlContent = post.html;
      let hugoContent = post.html;
      // put here to think about...in case this is ever needed...
      // const postOriginal = post;

      // establish the archiving paths (we have 3 possible)
      let hugoPathBase = path.join( archivePathBase, '_hugo', hugo.GetArchiveBundlePath(post) ); // hugo always goes to the same place
      let pathExtension = ''; // value adjusted according to archive selection made by the user
      let htmlPathExtension = ''; // in case we need a bundle on the end just for html
      let assetPathExtension = '';
      let tagPath = '';
      let datePath = '';
      const splitDate = post.published_at.split('-');
      let dateYear  = splitDate[0];
      let dateMonth = splitDate[1];
      let dateDay   = splitDate[2].split('T')[0];

      // if i was smart I would have come up with a way to track the order of arguments
      // then I could enable more fine grained ways of adjusting archive pathing
      if(ARCHIVE_TAG)
      {
        let tempTag = ( post.primary_tag ? post.primary_tag.name : '' );
        tempTag.replace('#', '');
        tagPath = tempTag;
      }
      if(ARCHIVE_DAY)
      {
        datePath = path.join( dateYear, dateMonth, dateDay);
      }
      else if(ARCHIVE_MMDD)
      {
        datePath = path.join( dateYear, dateMonth.toString()+dateDay.toString());
      }
      else if(ARCHIVE_MONTH)
      {
        datePath = path.join( dateYear, dateMonth);
      }
      else if(ARCHIVE_YEAR)
      {
        datePath = dateYear;
      }
      
      let bundleDir = "";
      if(ARCHIVE_FLAT==false)
          bundleDir = post.slug;
      if(ARCHIVE_SEPARATE)
      {
        pathExtension = path.join( tagPath, datePath, bundleDir );
        assetPathExtension = path.join( 'assets', pathExtension);
        htmlPathExtension = path.join( 'html', pathExtension);
      }
      else
      {
        pathExtension = path.join( tagPath, datePath, bundleDir );
        assetPathExtension = pathExtension;
        htmlPathExtension = pathExtension;
      }

      // RESOURCE EXTRACTION for HTML and HUGO ----------------------------------------------------------------
      if(TYPE_HTML || TYPE_HUGO || TYPE_ASSETS)
      {
        // create directories as a bundle so that all resources are stored with the post
        const htmlPathBase  = path.join(archivePathBase, htmlPathExtension);
        const htmlPath      = path.join(htmlPathBase, `${post.slug}`+'.html');
        if(TYPE_HTML)
        {
          fs.mkdirSync(htmlPathBase, { recursive: true });
        }
        const assetPathBase = path.join(archivePathBase, assetPathExtension);
        let htmlPathR = [];
        let hugoPathR = [];
        let asstPathR = [];
        // create the asset folders for the post (if needed)
        ASSET_TYPES.forEach( assetType =>
        {
          if( TYPE_ASSETS )
          {
            asstPathR[assetType] = path.join(assetPathBase, assetType);
            if(!fs.existsSync(asstPathR[assetType]))
            {
              directoriesToClean.push(asstPathR[assetType]);
              fs.mkdirSync(asstPathR[assetType], { recursive: true });
            }
          }
          else if( TYPE_HTML )
          {
            htmlPathR[assetType] = path.join(htmlPathBase, assetType);
            if(!fs.existsSync(htmlPathR[assetType]))
            {
              directoriesToClean.push(htmlPathR[assetType]);
              fs.mkdirSync(htmlPathR[assetType], { recursive: true });
            }
          }
          if( TYPE_HUGO )
          {
            hugoPathR[assetType] = hugo.GetPostResourcePath(assetType, hugoPathBase);
            if(!fs.existsSync(hugoPathR[assetType]))
            {
              directoriesToClean.push(hugoPathR[assetType]);
              fs.mkdirSync(hugoPathR[assetType], { recursive: true });
            }
          }
        });


        // RESOURCE EXTRACTION

        // HTML RESOURCE
        // collect all urls to files, images, etc...
        let fileRefs = getAllFileRefsFromHtml(htmlContent);
        // convert to local paths, replace in HTML, and copy to bundle
        fileRefs.forEach( oldFile =>
        {
          if (oldFile.url.includes(ghostURL) 
              // && ASSET_TYPES.includes(oldFile.type) // when testing regex on 'misc'
            )
          {
            let fileName = path.basename(oldFile.url);

            // tracking for garbage collection
            if(TYPE_ASSETS)
            {
              let i = directoriesToClean.indexOf(asstPathR[oldFile.type])
              if(i>-1)
                directoriesToClean.splice(i, 1);
            }
            else if(TYPE_HTML)
            {
              let i = directoriesToClean.indexOf(htmlPathR[oldFile.type])
              if(i>-1)
                directoriesToClean.splice(i, 1);
            }
            if(TYPE_HUGO)
            {
              let i = directoriesToClean.indexOf(hugoPathR[oldFile.type]);
              if(i>-1)
                directoriesToClean.splice(i, 1);
            }

            // we need to rename resized images
            if(oldFile.url.includes("images/size/"))
            {
              // /size/w1000/
              let regex =  RegExp('.*?/size/(.*?)/', 'g');
              let match = regex.exec( oldFile.url )
              if (match != null)
              {
                tempFileName = fileName.split(".");
                fileName = tempFileName[0]+match[1]+"."+tempFileName[1]
              }
            }

            let relativeFileUrl = "./"+oldFile.type+"/" + fileName;
            if(ARCHIVE_SEPARATE)
            {
              if( TYPE_ASSETS )
              {
                relativeFileUrl = path.join( path.relative( assetPathBase, archivePathBase),"assets",(pathExtension.replace("\\","/")),"images",fileName);
              }
              else if ( TYPE_HTML )
              {
                relativeFileUrl = path.join( path.relative( htmlPathBase, archivePathBase),"html",(pathExtension.replace("\\","/")), "images", fileName);
              }
              else if ( TYPE_HUGO )
              {
                relativeFileUrl = path.join( path.relative( htmlPathBase, archivePathBase),"_hugo","posts",post.slug, "images", fileName);
              }
            }
            htmlContent = htmlContent.replaceAll(oldFile.url, relativeFileUrl);
            hugoContent = hugoContent.replaceAll(oldFile.url, hugo.GetPostResourceUrl(oldFile.type, fileName, hugoPathBase));


            let newFilePath = '';
            if (TYPE_ASSETS)
              newFilePath = path.join(asstPathR[oldFile.type], fileName);
            else if(TYPE_HTML)
              newFilePath = path.join(htmlPathR[oldFile.type], fileName);
            else if (TYPE_HUGO)
              newFilePath = path.join(hugoPathR[oldFile.type], fileName);

            const streamedFile = fs.createWriteStream(newFilePath);

            let protocol = oldFile.url.indexOf("https")==0 ? https : http;
            const request = protocol.get(oldFile.url, function(response)
            {
              response.pipe(streamedFile);
              // after download completed close filestream
              streamedFile.on("finish", () =>
              {
                // after download completed close filestream
                streamedFile.close();
                // We may need to copy to assets...
                if((TYPE_HTML||TYPE_ASSETS) && TYPE_HUGO)
                {
                  // copy to hugo
                  fs.copySync(newFilePath, path.join(hugoPathR[oldFile.type], fileName) );
                }
              });
            });
          }
        });

        // META IMAGES
        // Process all of the Post's meta img's by iterating over the kinds
        // we copy them and change the reference in Post to reflect the change if they exist
        const metaImages = ['feature_image','og_image','twitter_image'];
        metaImages.forEach( (metaImageName) =>
        {
          if(post[metaImageName] && post[metaImageName].includes(ghostURL))
          {
            // used for resource tracking (we delete folders we don't need)
            if(TYPE_ASSETS)
            {
              let i = directoriesToClean.indexOf(asstPathR["images"])
              if(i>-1)
                directoriesToClean.splice(i, 1);
            }
            else if(TYPE_HTML)
            {
              let i = directoriesToClean.indexOf(htmlPathR["images"])
              if(i>-1)
                directoriesToClean.splice(i, 1);
            }
            if(TYPE_HUGO)
            {
              let i = directoriesToClean.indexOf(hugoPathR["images"]);
              if(i>-1)
                directoriesToClean.splice(i, 1);
            }
            // adjust post's JSON since we are copying the image to local
            const oldImgUrl = post[metaImageName];
            const imageName = path.basename(oldImgUrl);
            // only needed for hugo
            post[metaImageName] = hugo.GetPostResourceUrl("images", imageName, hugoPathBase);

            let newImgPath = "";
            if (TYPE_ASSETS)
              newImgPath = path.join(asstPathR["images"], imageName);
            else if(TYPE_HTML)
              newImgPath = path.join(htmlPathR["images"], imageName);
            else if (TYPE_HUGO)
              newImgPath = path.join(hugoPathR["images"], imageName);

            // create the stream
            const streamedFile = fs.createWriteStream(newImgPath);
            let protocol = oldImgUrl.indexOf("https")==0 ? https : http;
            const request = protocol.get(oldImgUrl, function(response)
            {
              response.pipe(streamedFile);
              streamedFile.on("finish", () =>
              {
                // after download completed close filestream
                streamedFile.close();
                // We may need to copy to assets...
                if((TYPE_HTML||TYPE_ASSETS) && TYPE_HUGO)
                {
                  // copy to hugo
                  fs.copySync(newImgPath, path.join(hugoPathR["images"], imageName) );
                }
              });
            });
          }
        });

        // HTML OUTPUT --------------------------------------------------------------------
        if(TYPE_HTML)
        {
          // feature_image is a special case because we display it in the HTML, if there is a feature_image
          let featureImageHtml = "";
          if(post["feature_image"])
          {
            featureImageHtml = "<img style=\"width:800px; height:150px; object-fit:cover;\" src=\""+post["feature_image"]+"\">"+EL+EL
          }
          // translate publish date into a human readable string
          const publishDate = new Date(post.published_at).toLocaleDateString('en-us', { weekday:"long", year:"numeric", month:"short", day:"numeric"});
          const htmlFileString =
             "<!DOCTYPE html>"+EL
            +"<html><head><title>"+post.title+"</title>"+EL
            +"<link rel=\"stylesheet\" type=\"text/css\" href=\""+path.relative(htmlPathBase,archivePathBase)+"/style.css\">"+EL
            +"</head><body class=\"post-template is-head-left-logo has-cover is-dropdown-loaded\">"+EL
            +"<div class=\"viewport\">"+EL
            +"  <div class=\"site-content\">"+EL
            +"    <main id=\"site-main\" class=\"site-main\">"+EL
            +"      <article class=\"article post\">"+EL
            +"        <header class=\"article-header gh-canvas\">"+EL
            + featureImageHtml
            +"          <h1>"+post.title+"</h1>"+EL
            +"          <h4>"+post.primary_author.name+"</h4>"+EL
            +"          <h4>"+publishDate+"</h4>"+EL
            +"        </header>"+EL
            +"        <section class=\"gh-content gh-canvas\">"+EL
            +""+EL
            // would be nice to pretty print this
            +"\t"+htmlContent+""+EL
            +""+EL
            +"        </section>"+EL
            +"      </article>"+EL
            +"    </main>"+EL
            +"  </div>"+EL
            +"</div></body></html>"+EL;
          await fs.writeFile(htmlPath, htmlFileString, { flag: 'w' });
        }
        // --------------------------------------------------------------------------------
      }
      // - END RESOURCE EXTRACTION ---------------------------------------------------------

      // TEXT OUTPUT ----------------------------------------------------------------
      let textPathBase = "";
      if (!ARCHIVE_SEPARATE && (TYPE_MD||TYPE_JSON||TYPE_TXT))
      {
        textPathBase = path.join(archivePathBase, pathExtension);
        if (!fs.existsSync(textPathBase)) { fs.mkdirSync(textPathBase, { recursive: true }); }
      }

      if (TYPE_HUGO)
      {
        await fs.writeFile(
          path.join(hugoPathBase, hugo.GetPostFilename(post.slug)),
          hugo.CompileHugoFileText(post,hugoContent),
          { flag: 'w' }
        );
      }

      let textFiles = [];
      if (TYPE_MD)
      {
        const turndownService = new TurndownService();
        let processedHtml = processHtmlForMarkdown(htmlContent);
        const markDownContent = turndownService.turndown(processedHtml);

        textFiles.push(
        {
          ext:      "md",
          content:  "---"+EL
                  + "title: "  + post.title +EL
                  + "author: " + post.primary_author.name +EL
                  + "date: "   + post.published_at +EL
                  + "image: "  + post.feature_image +EL
                  + "---"+EL+EL
                  + markDownContent
        })
      }
      if (TYPE_JSON)
      {
        // update just in case
        let postJson = post;
        postJson.html = htmlContent
        textFiles.push(
        {
          ext:    "json",
          content: JSON.stringify(postJson, null, 2)
        })
      }
      if (TYPE_TXT)
      {
        textFiles.push(
        {
          ext:    "txt",
          content: post.plaintext
        })
      }

      if(textFiles.length)
      {
        textFiles.forEach( async (file) =>
        {
          let thisPath = "";
          if (ARCHIVE_SEPARATE)
          {
            thisPath = path.join(archivePathBase, file.ext, pathExtension);
            if (!fs.existsSync(thisPath)) { fs.mkdirSync(thisPath, { recursive: true }); }
          }
          else
          {
            thisPath = textPathBase;
          }
          await fs.writeFile(path.join(thisPath, post.slug+"."+file.ext), file.content, { flag: 'w' });
        });
      }

      // --------------------------------------------------------------------------------

    }));
    console.timeEnd('All posts exported in');

    // garbage collection
    deleteEmptyDirectoriesInList(directoriesToClean);
  }
  catch (error)
  {
    console.error("\x1b[31mERROR"+C.Reset);
    if(error.code == "ECONNREFUSED")
    {
      console.error(error.code + " - " + "Your connection to "+ghostURL+" failed. Is the URL correct? Is the server running?"+EL);
    }
    else if(error.code == "UNKNOWN_CONTENT_API_KEY")
    {
      console.error(error.type +" "+error.code +" - "+"your connection to "+ghostURL+" was rejected. Is your Content API Key correct?"+EL);
    }
    else
    {
      console.error(error);
    }

    deleteDirectory(archivePathBase, true);
  }
};

module.exports = archiveGhostBlog();