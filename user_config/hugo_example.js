//////////////////////////
// hugo_example.js
//
// hugo.js is the file you want to configure. hugo_example.js is the example template.
// npm install copies hugo_example.js to hugo.js if hugo.js is missing.


const path = require('path');

/// ------ private utility functions -----------
// recommend that you don't mess with the private utility function(s)
// but changes here won't break code in other files so have at it if you want

const getTagNames = (post) =>
{
  let tagList = [];
  let hiddenList = [];
  post?.tags.forEach( tag =>
  {
    // ghost has hidden tags. those can be useful
    // so let's store them in a separate array
    if ( tag.name.indexOf("#")==0 )
      hiddenList.push(tag.name);
    else
      tagList.push(tag.name);
  })
  return {tags: tagList, hiddenTags: hiddenList};
}


/// -------- configurable exports --------------

// returns a path
// unique for the archive bundle of this particular post
// the returned path appends to: <archivePathBase>/_hugo/
module.exports.GetArchiveBundlePath = (post) =>
{
  return path.join('posts', `${post.slug}`);
}

// returns a string (filename)
// the most interesting change here would probably be the extension
module.exports.GetPostFilename = (slug) =>
{
  return 'index.html';
}

// returns a path
// unique for the resource directory of this particular post
// note that the resoureType string doubles as the directory name
// hugoArchiveBundlePath by default ends in the slug name for the post
//
// the retuned path can be anything you want so be careful
// the default bundles the post's images directory with the post itself
//    deviation from this requires altering the img srcs in the html too
module.exports.GetPostResourcePath = ( resourceType, hugoArchiveBundlePath ) =>
{
  return path.join(`${hugoArchiveBundlePath}`, `${resourceType}`);
}

// returns a url string for use in HTML
// unique for this resource of this post which by default is identifiable by the last name of hugoArchiveBundlePath
// note that the resourceType string is doubling as a directory name for me
//
// if you edit the function above, you will definitely want to edit this one
// i am basically forcing you to use a lookup table keyed to the resourceName or postSlug... or similar,
// but maybe you'll come up with something else i didn't think of
module.exports.GetPostResourceUrl = ( resourceType, resourceName, hugoArchiveBundlePath ) =>
{
  return ('./'+resourceType+'/'+resourceName);
}

// (this is a private function for your use so... have at it)
// returns stringified pretty frontmatter in json format for HUGO
// but you can change that as this function is only called by CompileHugoFileText
const createFrontmatterFromGhost = (post) =>
{
  let categories = getTagNames(post);

  let frontmatter =
  {
    // -- default hugo schema --
    title: post?.title,//                                          string
    slug: post?.slug,//                                            string
    summary: post?.custom_excerpt,//                               string
    date: post?.created_at,//                                      string
    publishDate: post?.published_at,//                             string
    lastmod: post?.published_at,//                                 string
    // description: post?.meta_description,//                         string
    tags: categories.tags,//                                    []string
  //   "keywords"              string
  //   "aliases"               []string
  //   "audio"                 []string
  //   "cascade"               []CascadeTarget
    draft: (post?.visibility!=='public'),//                        bool
  //   "expiryDate"            string
  //   "headless"              bool
  //   "images"                []string
  //   "isCJKLanguage"         bool
  //   "layout"                string
  //   "markup"                string
  //   "outputs"               string
  //   "series"                []string
  //   "type"                  string
  //   "url"                   string
  //   "videos"                []string
  //   "weight"                int
  }

  return JSON.stringify(frontmatter, null, 2);
}

// returns a string
// assemble your frontmatter and html content here.
// post is the JSON from ghost and hugoContent the HTML from ghost.
// Each has been changed prior to this function to alter the src references of images etc...
// Once output from here, the file is written.
module.exports.CompileHugoFileText = ( post, hugoContent ) =>
{
  const frontmatter = createFrontmatterFromGhost(post);

  const hugoFileText = 
      frontmatter
    + "\n"
    + hugoContent

  return hugoFileText;
}
