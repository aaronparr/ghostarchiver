#!/usr/bin/env node

// PREINSTALL SCRIPT
// create config files before running npm install

const fs = require('fs-extra');
const path = require('path');
const EL = require('os').EOL;
const C = require('./cliColors.js').Colors;

const preInstall = async () =>
{
  // init
  const env_file = path.join(__dirname,"..",".env");
  const hugo_config_file = path.join(__dirname,"..","user_config","hugo.js");
  const hugo_example_file = path.join(__dirname,"..","user_config","hugo_example.js");

  const style_file = path.join(__dirname,"..","user_config","style.css");
  const style_example_file = path.join(__dirname,"..","user_config","style_example.css");
  const style_script = path.join(__dirname,"..","user_config","style.js");
  const style_example_script = path.join(__dirname,"..","user_config","style_example.js");

  
  // make the .env if needed
  if(!fs.existsSync(env_file))
  {
    let envStream = fs.createWriteStream(env_file);
    envStream.write("GHOST_URL="+EL);
    envStream.write("GHOST_KEY="+EL);
    envStream.end();
    console.warn(C.Red+" !!!"+C.Reset);
    console.warn(C.Red+" !!! WARNING: "+C.Reset+"You need to edit .env with your credentials. See the README for more info.");
    console.warn(C.Red+" !!!"+C.Reset);
  }

  // create hugo.js from hugo_example.js if needed
  if(!fs.existsSync(hugo_config_file))
  {
    if(!fs.existsSync(hugo_example_file))
    {
      console.error(C.Red+" !!! ERROR: "+C.Reset+""+hugo_example_file+" is missing. Pull from the repository and try npm install again.");
      process.exit(1);
    }
    
    fs.copyFileSync(hugo_example_file, hugo_config_file);
    if(!fs.existsSync(hugo_config_file))
      console.warn(C.Red+" !!! WARNING: "+C.Reset+"Copy of "+hugo_example_file+" failed. You can copy it manually and name it hugo.js");
  }

  // create style css if needed
  if(!fs.existsSync(style_file))
  {
    if(!fs.existsSync(style_example_file))
    {
      console.error(C.Red+" !!! ERROR: "+C.Reset+style_example_file+" is missing. Pull from the repository and try npm install again.");
      process.exit(1);
    }
    
    fs.copyFileSync(style_example_file, style_file);
    if(!fs.existsSync(style_file))
      console.warn(C.Red+" !!! WARNING: "+C.Reset+"Copy of "+style_example_file+" failed. You can copy it manually and name it style.css");
  }
}

module.exports = preInstall();